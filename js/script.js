function addTask(task, desc, start, end, priority) {
	var taskArray = JSON.parse(localStorage.tasks);
	var data = ['false', task, desc, start, end, priority];
	taskArray.push(data);
	localStorage.tasks = JSON.stringify(taskArray);
	window.location.assign("index.html");
}

function deleteTask(id) {
	var taskArray = JSON.parse(localStorage.tasks);
	taskArray.splice(id, 1);
	localStorage.tasks = JSON.stringify(taskArray);
	window.location.assign("index.html");
}

function changeDone(id) {
	var str = id.substring(5);
	var taskArray = JSON.parse(localStorage.tasks);
	if (document.getElementById(id).checked) {
		taskArray[str][0] = 'true';
	}
	else {
		taskArray[str][0] = 'false';
	}
	localStorage.tasks = JSON.stringify(taskArray);
}

function editTask(id, task, desc, start, end, priority) {
	var taskArray = JSON.parse(localStorage.tasks);
	var realID = id.substring(4);
	console.log(realID);
	console.log(id);
	taskArray[realID][1] = task;
	taskArray[realID][2] = desc;
	taskArray[realID][3] = start;
	taskArray[realID][4] = end;
	taskArray[realID][5] = priority;

	localStorage.tasks = JSON.stringify(taskArray);
	window.location.assign("index.html");
}

function getTasks() {
	var task = JSON.parse(localStorage.tasks);
	$("#task-content").html("");
	for (var i = 0; i < task.length; i++) {
		$("#task-content").append("<tr>");
		$("#task-content").append("<td>"+(i + 1)+"</td><td><input type='checkbox' onchange='changeDone(this.id)' id='check"+i+"'></td><td>"+task[i][1]+"</td><td>"+task[i][2]+"</td><td>"+task[i][3]+"</td><td>"+task[i][4]+"</td><td>"+task[i][5]+"</td><td><button id='"+i+"' onclick='deleteTask(this.id)'>Delete</button></td><td><button data-toggle='modal' data-target='#updateTask' id='edit"+i+"' onclick='setUpdateData(this.id, '"+task[i][1]+"', '"+task[i][2]+"', '"+task[i][3]+"', '"+task[i][4]+"', '"+task[i][5]+"')'>Edit</button></td>");

		if (task[i][0] == 'true') {
			$("#check"+i).prop('checked', true);
		}
		$("#task-content").append("</tr>");
	}	
}

function setUpdateData(id, task, desc, start, end, priority) {
	$("#id-selected").val(id);
	$("#update-task").val(task);
	$("#update-desc").val(desc);
	$("#update-start").val(start);
	$("#update-end").val(end);
	$("#update-priority").val(priority);
}

$(document).ready(function() {
	if (!localStorage.tasks) {
		localStorage.tasks = '[]';
	}
	else {
		getTasks();
	}
});